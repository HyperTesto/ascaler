import java.io.File;
import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import allocation.Allocation;
import metrics.Metric;
import platform.Platform;
import scaling.Scaling;


public class Launcher {
	
	/**
	 * Launcher for the program
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("-----------------");
		System.out.println("Starting AAScaler");
		System.out.println("-----------------");
		System.out.println("Looking for aascaler.conf in default directory");
		
		ObjectMapper mapper = new ObjectMapper();
		
		try
        {
            //Convert Map to JSON
            
            File conf = new File("aascaler.conf");
            Configuration configuration;
            if (conf.exists()){
            	configuration = mapper.readValue(conf, Configuration.class);
                System.out.println("configuration correctly read!");
                Metric metric = PlugInFactory.initMetric(configuration);
                Allocation allocation = PlugInFactory.initAllocation(configuration);
                Scaling scaling = PlugInFactory.initScaling(configuration);
                Platform platform = PlugInFactory.initPlatform(configuration);
                AAScaler scaler = new AAScaler(allocation, metric, platform, scaling);
                scaler.start();
            }else{
            	System.err.println("Config file not found!");
            }
            
        } 
        catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

}
