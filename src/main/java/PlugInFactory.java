import allocation.Allocation;
import allocation.SplitMerge;
import allocation.StaticAllocation;
import metrics.InfluxDBMeterics;
import metrics.Metric;
import platform.DockerMachineDigitalOcean;
import platform.Platform;
import scaling.CPUAverage;
import scaling.CPUMovingAverage;
import scaling.Scaling;

/**
 * Hold all the logic for init the various plug-in that compose 
 * the program.
 * @author hypertesto
 *
 */
public class PlugInFactory {
	
	/**
	 * Init an allocation plug-in
	 * @param c
	 * @return
	 */
	public static Allocation initAllocation(Configuration c){
		System.out.println("--- INIT ALLOCATION ---");
		String alloc = c.getAllocation();
		Allocation a = null;
		System.out.println(alloc);
		switch (alloc){
		case SplitMerge.NAME:
			a = new SplitMerge();
			a.init(c.getAllocationOpt());
			break;
		case StaticAllocation.NAME:
			a = new StaticAllocation();
			a.init(c.getAllocationOpt());
		default:
			System.err.println("Unrecognized allocation: " + alloc);
			new Exception("Unrecognized alloc");
		}
		return a;
	}
	
	/**
	 * Init a metric plug-in
	 * @param c
	 * @return
	 */
	public static Metric initMetric(Configuration c){
		System.out.println("--- INIT METRICS ---");
		Metric res = null;
		String metric = c.getMetrics();
		System.out.println(metric);
		switch(metric){
		//metrics from influxDB
		case InfluxDBMeterics.NAME:
			res = new InfluxDBMeterics();
			res.init(c.getMetricsOpt());
			break;
		default:
			System.err.println("Unrecognized metric: " + metric);
			new Exception("Unrecognized metric");
		}
		
		return res;
		
	}
	
	/**
	 * Init a platform (provider) plug-in
	 * @param c
	 * @return
	 */
	public static Platform initPlatform(Configuration c){
		System.out.println("--- INIT PLATFORM ---");
		Platform res = null;
		String platform = c.getProvider();
		System.out.println(platform);
		switch(platform){
		case DockerMachineDigitalOcean.NAME:
			res = new DockerMachineDigitalOcean();
			res.init(c.getProviderOpt());
			break;
		default:
			System.err.println("Unrecognized provider: " + platform);
			new Exception("Unrecognized provider");
		}
		return res;
	}
	
	/**
	 * Init a scaling unit plug-in
	 * @param c
	 * @return
	 */
	public static Scaling initScaling(Configuration c){
		System.out.println("--- INIT SCALING ---");
		String scalingUnit = c.getScaling();
		Scaling s = null;
		System.out.println(scalingUnit);
		switch(scalingUnit){
		case CPUAverage.NAME:
			
			s =new CPUAverage();
			s.init(c.getScalingOpt());
			break;
		default:
			System.err.println("Unrecognized scaling unit: " + scalingUnit);
			new Exception("Unrecognized scaling unit");
		}
		return s;
	}
	

}
