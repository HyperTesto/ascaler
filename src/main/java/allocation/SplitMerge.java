package allocation;

import java.util.HashMap;

import metrics.Metric;
import scaling.Action;

public class SplitMerge implements Allocation{
	
	public static final String NAME = "split_merge";

	@Override
	public void init(HashMap<String, String> opt) {
		// this method has no configurations
		
	}

	@Override
	public int calculateContainers(int current,double avg, Action action) {
		String actionType = action.getAction();
		int res = -1;
		switch (actionType){
		case Action.type.SCALE_DOWN:
			res = current /2;
			break;
		case Action.type.SCALE_DOWN_DOWN:
			res = (int) (current /2.5);
			break;
		case Action.type.SCALE_UP:
			res = current * 2;
			break;
		case Action.type.SCALE_UP_UP:
			res = (int) (current * 2.5);
			break;
		default:
			new Exception("Unrecognized scaling requested");	
		}
		if (res < 4) { res = 4;}
		return res;
	}

	@Override
	public int calculateVMs(int current, double avg, Action action) {
		String actionType = action.getAction();
		int res = -1;
		switch (actionType){
		case Action.type.SCALE_DOWN:
			res = current /2;
			break;
		case Action.type.SCALE_DOWN_DOWN:
			res = (int) (current /2.5);
			break;
		case Action.type.SCALE_UP:
			res = current * 2;
			break;
		case Action.type.SCALE_UP_UP:
			res = (int) (current * 2.5);
			break;
		default:
			new Exception("Unrecognized scaling requested");	
		}
		if (res < 2) {res = 2;}
		return res;
	}

}
