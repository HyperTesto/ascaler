package allocation;

import java.util.HashMap;

import metrics.Metric;
import scaling.Action;

/**
 * Static allocation defined in configuration
 * @author hypertesto
 *
 */
public class StaticAllocation implements Allocation {
	
	public static final String NAME = "staticAllocation";
	
	int machineToAdd;
	int machineToRemove;
	int containerToAdd;
	int containerToRemove;

	@Override
	public int calculateContainers(int current, double avg, Action action) {
		String actionType = action.getAction();
		int res = -1;
		switch (actionType){
		case Action.type.SCALE_DOWN:
			res = current - containerToRemove;
			break;
		case Action.type.SCALE_DOWN_DOWN:
			res = current - containerToRemove;
			break;
		case Action.type.SCALE_UP:
			res = current + containerToAdd;
			break;
		case Action.type.SCALE_UP_UP:
			res = current + containerToAdd;
			break;
		default:
			new Exception("Unrecognized scaling requested");	
		}
		return res;
	}

	@Override
	public int calculateVMs(int current, double avg, Action action) {
		String actionType = action.getAction();
		int res = -1;
		switch (actionType){
		case Action.type.SCALE_DOWN:
			res = current - machineToRemove;
			break;
		case Action.type.SCALE_DOWN_DOWN:
			res = current - machineToRemove;
			break;
		case Action.type.SCALE_UP:
			res = current + machineToAdd;
			break;
		case Action.type.SCALE_UP_UP:
			res = current + machineToAdd;
			break;
		default:
			new Exception("Unrecognized scaling requested");	
		}
		return res;
	}

	@Override
	public void init(HashMap<String, String> opt) {
		
		machineToAdd = Integer.parseInt(opt.get(options.VM_ADD));
		machineToRemove = Integer.parseInt(opt.get(options.VM_REMOVE));
		containerToAdd = Integer.parseInt(opt.get(options.CONTAINER_ADD));
		containerToRemove =Integer.parseInt(opt.get(options.CONTAINER_REMOVE));
		
	}
	
	public static final class options {
		
		public static final String VM_ADD = "machineToAdd";
		public static final String VM_REMOVE = "machineToRemove";
		public static final String CONTAINER_ADD = "containerToAdd";
		public static final String CONTAINER_REMOVE = "containerRemove";
		
	}

}
