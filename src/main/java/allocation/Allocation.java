package allocation;

import java.util.HashMap;

import metrics.Metric;
import scaling.Action;

public interface Allocation {
	
	public void init(HashMap<String, String> opt);
	
	public int calculateContainers(int current, double avg, Action action);
	
	public int calculateVMs(int current, double avg, Action action);

}
