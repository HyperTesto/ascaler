import java.util.HashMap;
import java.util.Hashtable;

/**
 * Class for mapping configuration file
 * @author hypertesto
 *
 */
public class Configuration {
	
	private String provider;
	private HashMap<String, String> providerOpt;
	private String scaling;
	private HashMap<String, String> scalingOpt;
	private String metrics;
	private HashMap<String, String> metricsOpt;
	private String allocation;
	private HashMap<String, String> allocationOpt;
	
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public HashMap<String, String> getProviderOpt() {
		return providerOpt;
	}
	public void setProviderOpt(HashMap<String, String> providerOpt) {
		this.providerOpt = providerOpt;
	}
	public String getScaling() {
		return scaling;
	}
	public void setScaling(String scaling) {
		this.scaling = scaling;
	}
	public HashMap<String, String> getScalingOpt() {
		return scalingOpt;
	}
	public void setScalingOpt(HashMap<String, String> scalingOpt) {
		this.scalingOpt = scalingOpt;
	}
	public String getMetrics() {
		return metrics;
	}
	public void setMetrics(String metrics) {
		this.metrics = metrics;
	}
	public HashMap<String, String> getMetricsOpt() {
		return metricsOpt;
	}
	public void setMetricsOpt(HashMap<String, String> metricsOpt) {
		this.metricsOpt = metricsOpt;
	}
	public String getAllocation() {
		return allocation;
	}
	public void setAllocation(String allocation) {
		this.allocation = allocation;
	}
	public HashMap<String, String> getAllocationOpt() {
		return allocationOpt;
	}
	public void setAllocationOpt(HashMap<String, String> allocationOpt) {
		this.allocationOpt = allocationOpt;
	}

}
