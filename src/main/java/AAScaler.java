import allocation.Allocation;
import metrics.Metric;
import platform.Platform;
import scaling.Scaling;

/**
 * Central class for working algorithms.
 * It runs the choosen scaling alg ulsess it's manually stopped
 * @author hypertesto
 *
 */
public class AAScaler extends Thread{
	
	private Allocation allocation;
	private Metric metric;
	private Platform platform;
	private Scaling scaling;
	
	public AAScaler(Allocation allocation,
					Metric metric,
					Platform platform,
					Scaling scaling){
		
		this.allocation = allocation;
		this.metric = metric;
		this.platform = platform;
		this.scaling = scaling;
		
	}
	
	@Override
	public void run(){
		
		while (true) {
			
			scaling.evalAndExecute(metric, allocation, platform);
			try {
			Thread.sleep(30000); //check every 30 secs
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
	}

}
