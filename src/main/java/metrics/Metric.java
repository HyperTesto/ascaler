package metrics;

import java.util.HashMap;
import java.util.Hashtable;

public interface Metric {
	
	public void init (HashMap<String, String> parameters);
	
	public double getLastMinuteAVGContainer();
	
	public double[] getLastMinuteSerieContainer();
	
	public double getLastMinuteAVGVM();
	
	public double[] getLastMinuteSerieVM();
	
}
