package metrics;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Pong;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;
import org.influxdb.dto.QueryResult.Series;

public class InfluxDBMeterics implements Metric {
	
	public static final String NAME = "influxdb";
	
	String ip;
	String port;
	String user;
	String password;
	InfluxDB influxDB;
	

	@Override
	public void init(HashMap<String, String> parameters) {
		
		ip = parameters.get(options.IP);
		port = parameters.get(options.PORT);
		user = parameters.get(options.USER);
		password = parameters.get(options.PASSWORD);
		
		testConnection();
		influxDB = InfluxDBFactory.connect("http://" + ip + ":" + port, user, password);

	}

	@Override
	public double getLastMinuteAVGVM() {
		String queryString = "SELECT (mean(\"usage_user\") +  mean(\"usage_system\"))  FROM \"cpu\" "
				+ "WHERE time > now() - 1m GROUP BY \"host\" fill(null)";
		Query query = new Query(queryString, "telegraf");
		QueryResult result = influxDB.query(query);
		
		System.out.println(result);	
		System.out.println("Size: " + result.getResults().size());
		
		Result singleResult = result.getResults().get(0);
		
		
		Series serie = singleResult.getSeries().get(0);
		int count = 0;
		double sum = 0;
		for (Series s : singleResult.getSeries()){
			if(s.getTags().get("host").contains("node")){
				List<Object> measureList =  s.getValues().get(0);
				System.out.println("values: " + measureList);
				
				//here we expect the mean to be in the 2nd column
				double res = (double) measureList.get(1);
				sum+= res;
				count++;
				
			}
		}
		
		
		return sum / count;
	}

	@Override
	public double[] getLastMinuteSerieVM() {
		double res[] = new double[10];
		String queryString = "SELECT (mean(\"usage_user\") +  mean(\"usage_system\"))  FROM \"cpu\" "
				+ "WHERE time > now() - 10m GROUP BY time(1m), \"host\" fill(null)";
		Query query = new Query(queryString, "telegraf");
		QueryResult result = influxDB.query(query);
		
		System.out.println(result);	
		System.out.println("Size: " + result.getResults().size());
		
		Result singleResult = result.getResults().get(0);
		
		
		Series serie = singleResult.getSeries().get(0);
		int count = 0;
		double sum = 0;
		for (Series s : singleResult.getSeries()){
			if(s.getTags().get("host").contains("node")){
				List<Object> measureList =  s.getValues().get(0);
				System.out.println("values: " + measureList);
				System.out.print("lastTenMinutes [");
				//here we expect the mean to be in the 2nd column
				for (int i = 0; i < 10 ; i++){
					res[i] = (double) measureList.get(1);
					System.out.print(res[i] + "," );
				}
				

			}
		}
		System.out.print("]");
		System.out.println();
		return res;
		//return sum / count;
	}
	
	public static final class options{
		public static final String IP = "address";
		public static final String PORT = "port";
		public static final String USER = "user";
		public static final String PASSWORD = "password";
	}
	
	/**
	 * Test if the connection is OK, if not raise an exception
	 */
	private void testConnection(){
		InfluxDB databaseTest = InfluxDBFactory.connect("http://" + ip + ":" + port, user, password);
		Pong pong = databaseTest.ping();
		System.out.println(pong);
		//getContainerMovingAverage();
		
	}

	public double getContainerMovingAverage() {
		String queryString = "SELECT moving_average(\"usage_percent\",\"5m\") FROM \"docker_container_cpu\" "
				+ "WHERE time > now() - 1h GROUP BY \"container_image\" fill(null)";
		Query query = new Query(queryString, "telegraf");
		QueryResult result = influxDB.query(query);
		
		System.out.println(result);	
		System.out.println("Size: " + result.getResults().size());
		
		Result singleResult = result.getResults().get(0);
		
		for (Series s : singleResult.getSeries()){
			if(s.getTags().get("container_image").equals("hypertesto/hanami-fibo")){
				List<Object> measureList =  s.getValues().get(0);
				System.out.println("values: " + measureList);
				
				//here we expect the mean to be in the 2nd column
				double res = (double) measureList.get(1);
				System.out.println("AVG_MEAN: " + res);
				return res;
				
			}
		}

		return -1;
	}

	@Override
	public double getLastMinuteAVGContainer() {
		String queryString = "SELECT mean(\"usage_percent\") FROM \"docker_container_cpu\" "
				+ "WHERE time > now() - 1m GROUP BY \"container_image\" fill(null)";
		Query query = new Query(queryString, "telegraf");
		QueryResult result = influxDB.query(query);
		
		
		
		System.out.println(result);	
		System.out.println("Size: " + result.getResults().size());
		
		Result singleResult = result.getResults().get(0);
		
		
		Series serie = singleResult.getSeries().get(0);
		
		for (Series s : singleResult.getSeries()){
			if(s.getTags().get("container_image").equals("hypertesto/hanami-fibo")){
				List<Object> measureList =  s.getValues().get(0);
				System.out.println("values: " + measureList);
				
				//here we expect the mean to be in the 2nd column
				double res = (double) measureList.get(1);
				res = getLastMinuteAVGVM();
				System.out.println("AVG_MEAN: " + res);
				return res;
				
			}
		}

		return -1;
	}
	
	@Override
	public double[] getLastMinuteSerieContainer() {
		return getLastMinuteSerieVM(); //stats are equals so we can reuse the mothod
	}

}
