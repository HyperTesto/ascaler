import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import platform.DockerMachineDigitalOcean;

/*
 * This Java source file was auto generated by running 'gradle buildInit --type java-library'
 * by 'hypertesto' at '30/08/16 15.30' with Gradle 2.13
 * 
 * @modified: generate a stub configuration file to edit
 *
 * @author hypertesto, @date 30/08/16 15.30
 */
public class Library {
    public boolean someLibraryMethod() {
        return true;
    }
    
    public static void main(String[] args){
    	System.out.println("Generating stub file...");
    	Configuration c = new Configuration();
    	c.setProvider("digitalocean");
    	HashMap<String, String> providerOpt = new HashMap<>();
    	providerOpt.put(DockerMachineDigitalOcean.options.OS, "debian");
    	providerOpt.put(DockerMachineDigitalOcean.options.REGION, "ams2");
    	providerOpt.put(DockerMachineDigitalOcean.options.TOKEN, "dadsadasdasdas");
    	providerOpt.put(DockerMachineDigitalOcean.options.VM_TYPE, "512mb");
    	c.setProviderOpt(providerOpt);
    	c.setAllocation("split_merge");
    	c.setMetrics("influxdb");
    	HashMap<String, String> metricsOpt = new HashMap<>();
    	metricsOpt.put("address", "localhost");
    	metricsOpt.put("port", "localhost");
    	metricsOpt.put("database", "localhost");
    	metricsOpt.put("user", "localhost");
    	metricsOpt.put("password", "localhost");
    	c.setMetricsOpt(metricsOpt);
    	c.setScaling("cpu_average");
    	HashMap<String, String> scalingOpt = new HashMap<>();
    	scalingOpt.put("upThr", "70");
    	scalingOpt.put("botThr", "20");
    	scalingOpt.put("upCalmTime", "120");
    	scalingOpt.put("botCalmTime", "120");
    	scalingOpt.put("upUpThr", "90");
    	scalingOpt.put("botBotThr", "5");
    	scalingOpt.put("upUpCalmTime", "120");
    	scalingOpt.put("botBotCalmTime", "120");
    	c.setScalingOpt(scalingOpt);
    	
    	ObjectMapper mapper = new ObjectMapper();
    	try
        {
            //Convert Map to JSON
            
            File conf = new File("aascaler.conf");
            mapper.writerWithDefaultPrettyPrinter().writeValue(conf, c);
            
        } 
        catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 

    	
    }
}
