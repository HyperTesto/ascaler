package platform;

import java.io.File;
import java.lang.ProcessBuilder.Redirect;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * This class implements both a cloud provider api integration and
 * a container-engine integration.
 * In future work this will be separated and the work will be based on Ansible
 * @author hypertesto
 *
 */
public class DockerMachineDigitalOcean implements Platform {
	
	public static final String NAME = "dm_digitalocean";
	
	private String token;
	private String vmType;
	private String region;
	private String operatingSystem;
	private boolean privateNet;

	@Override
	public void init(HashMap<String, String> parameters) {
		token = parameters.get(options.TOKEN);
		vmType = parameters.get(options.VM_TYPE);
		region = parameters.get(options.REGION);
		operatingSystem = parameters.get(options.OS);
		privateNet = true; //right now we enforce it to stick with our infrastructure

	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getVmType() {
		return vmType;
	}

	public void setVmType(String vmType) {
		this.vmType = vmType;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}


	
	/**
	 * Options and parameters for the correct init of the class 
	 * @author hypertesto
	 *
	 */
	public static final class options {
		public static final String TOKEN = "token";
		public static final String VM_TYPE = "vmType";
		public static final String REGION = "region";
		public static final String OS = "os";
		public static final String PRIVATE_NET = "private_net";
	}



	@Override
	public void scaleUpContainer(int n, int current) {
		
		/*
		 * we need the fibonacci compose.yml located in /fibonacci (image is on Docker Hub)
		 */
		Process p;
		String command = "scale_container.sh";
		System.out.println(command);
		try {
			ProcessBuilder pb = new ProcessBuilder("/bin/bash", command, ""+n);
			 pb.directory(new File("/home/hypertesto"));
			 pb.redirectOutput(Redirect.INHERIT);
			 pb.redirectError(Redirect.INHERIT);
			 pb.start().waitFor();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void scaleDownContainer(int n, int current) {
		//the steps are the same, docker-swarm takes care of the rest
		if (n < 4){
			System.out.println("Reqested " + n + " but minimum is 4");
			n = 4;
		}
		if (current == n) {
			System.out.println("Already in the desidered state, skipping...");
		} else {
			System.out.println("Scaling down to " + n);
			try {
				Thread.sleep(60000); //docker complains right after the remove of a node
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			scaleUpContainer(n, current);
		}
		
	}

	@Override
	public void scaleUpVM(int n, int current) {
		
		System.out.println("Scaling up from " + current +" to " + n);
		String command = "scale_up_vm.sh";
		for (int i=current +1 ; i < n ; i++){
			Process p;
			try {
				
				System.out.println("creating node0" + i);
				ProcessBuilder pb = new ProcessBuilder("/bin/bash", command, ""+i);
				 pb.directory(new File("/home/hypertesto"));
				 pb.redirectOutput(Redirect.INHERIT);
				 pb.redirectError(Redirect.INHERIT);
				 pb.start().waitFor();
				 
				
		

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void scaleDownVM(int n, int current) {
		
		String command = "scale_down_vm.sh";
		
		for (int i=current ; i > n ; i--){
			if(i <= 2){
				System.out.println("Minimun number of VM reached, skipping...");
			} else {
				System.out.println("Scaling down node0" +i );
				Process p;
				try {
					/*
					 * Stops and then remove a machine from digitalocean using docker-machine
					 */
					ProcessBuilder pb = new ProcessBuilder("/bin/bash", command, ""+i);
					 pb.directory(new File("/home/hypertesto"));
					 pb.redirectOutput(Redirect.INHERIT);
					 pb.redirectError(Redirect.INHERIT);
					 pb.start().waitFor();
	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
	}

}
