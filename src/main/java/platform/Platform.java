package platform;

import java.util.HashMap;
import java.util.Hashtable;

public interface Platform {
	
	public void init (HashMap<String, String> parameters);
	
	public void scaleUpContainer(int n, int current);
	public void scaleDownContainer(int n, int current);
	public void scaleUpVM(int n, int current);
	public void scaleDownVM(int n, int current);

}
