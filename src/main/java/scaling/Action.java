package scaling;

public class Action{
	
	String action;
	
	public Action(String action){
		this.action = action;
	}
	
	public static final class type {
		
		public static final String SCALE_UP = "scaleUp";
		public static final String SCALE_DOWN = "scaleDown";
		public static final String SCALE_UP_UP = "scaleUpUp";
		public static final String SCALE_DOWN_DOWN = "scaleDownDown";
		
	}
	
	public String getAction(){
		return this.action;
	}

}
