package scaling;

import java.util.HashMap;

import allocation.Allocation;
import metrics.Metric;
import platform.Platform;

public interface Scaling {
	
	public void init (HashMap<String, String> opt);
	
	public void evalAndExecute(Metric m, Allocation a, Platform p);
}
