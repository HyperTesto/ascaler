package scaling;

import java.util.HashMap;
import java.util.Hashtable;

import allocation.Allocation;
import metrics.Metric;
import platform.Platform;
import scaling.CPUAverage.options;

public class CPUMovingAverage implements Scaling {
	
	public static final String NAME = "cpu_moving_average";
	
	/*
	 * This is our initial config
	 */
	public static int CURRENT_VM = 2;
	public static int CURRENT_CONTAINERS = 4;
	
	
	private int upThr;
	private int botThr;
	private int upCalmTime;
	private int botCalmTime;
	private int upUpThr;
	private int botBotThr;
	private int upUpCalmTime;
	private int botBotCalmTime;

	@Override
	public void init(HashMap<String, String> opt) {
		
		upThr = Integer.parseInt(opt.get(options.UP_THR));
		botThr = Integer.parseInt(opt.get(options.BOT_THR));
		upCalmTime = Integer.parseInt(opt.get(options.UP_CALM_TIME));
		botCalmTime = Integer.parseInt(opt.get(options.BOT_CALM_TIME));
		
		upUpThr = Integer.parseInt(opt.get(options.UP_UP_THR));
		botBotThr = Integer.parseInt(opt.get(options.BOT_BOT_THR));
		upUpCalmTime = Integer.parseInt(opt.get(options.UP_UP_CALM_TIME));
		botBotCalmTime = Integer.parseInt(opt.get(options.BOT_BOT_CALM_TIME));	
		
	}

	@Override
	public void evalAndExecute(Metric m, Allocation a, Platform p) {
		
		System.out.println("-- evalAndExecute --");
				
		double[] containerSerie = m.getLastMinuteSerieContainer();
		double hostSerie[] = m.getLastMinuteSerieVM();
		
		double sum = 0;
		for (int i = 0; i < 10 ; i++){
			sum+=containerSerie[i];
		}
		
		double moingAVG = sum / 10;
		
		/*
		 * First we check the worst case
		 */
		
		int vms;
		int containers;
		Action action;
		
		System.out.println("containerMovingAvg: " + moingAVG + " hostMoving: " + moingAVG);
		
		if (moingAVG >= upUpThr) {
			
			System.out.println("Scaling UP UP");
			
			action = new Action(Action.type.SCALE_UP_UP);
			vms =  a.calculateVMs(CURRENT_VM, moingAVG, action);
			containers = (int) (1.5 * a.calculateContainers(CURRENT_CONTAINERS, moingAVG, action));
			
			p.scaleUpVM(vms, CURRENT_VM);
			p.scaleUpContainer(containers, CURRENT_CONTAINERS);
			
			try {
				Thread.sleep(upUpCalmTime * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		} else if (moingAVG >= upThr) {
			
			System.out.println("Scaling UP");
			
			action = new Action(Action.type.SCALE_UP);
			vms = a.calculateVMs(CURRENT_VM, moingAVG, action);
			containers = a.calculateContainers(CURRENT_CONTAINERS, moingAVG, action);
			
			p.scaleUpVM(vms, CURRENT_VM);
			p.scaleUpContainer(containers, CURRENT_CONTAINERS);
			
			try {
				Thread.sleep(upCalmTime * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		} else if (moingAVG <= botBotThr ) {
			
			System.out.println("Scaling BOT BOT");
			
			action = new Action(Action.type.SCALE_DOWN_DOWN);
			vms = a.calculateVMs(CURRENT_VM, moingAVG, action);
			containers = a.calculateContainers(CURRENT_CONTAINERS, moingAVG, action);
			
			p.scaleDownVM(vms, CURRENT_VM);
			p.scaleDownContainer(containers, CURRENT_CONTAINERS);
			
			try {
				Thread.sleep(botBotCalmTime * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		} else if (moingAVG <= botThr) {
			
			System.out.println("Scaling BOT");
			
			action = new Action(Action.type.SCALE_DOWN);
			vms = a.calculateVMs(CURRENT_VM, moingAVG, action);
			containers = a.calculateContainers(CURRENT_CONTAINERS, moingAVG, action);
			
			p.scaleDownVM(vms, CURRENT_VM);
			p.scaleDownContainer(containers, CURRENT_CONTAINERS);
			
			try {
				Thread.sleep(botCalmTime * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else {
			
			System.out.println("Nothing!");
			//enforce an assignement since nothing changed
			vms = CURRENT_VM;
			containers = CURRENT_CONTAINERS;
		}
		
		CURRENT_VM = vms;
		CURRENT_CONTAINERS = containers;
		
	}
	
	public static final class options {
		public static final String UP_THR = "upThr";
		public static final String BOT_THR = "botThr";
		public static final String UP_UP_THR = "upUpThr";
		public static final String BOT_BOT_THR = "botBotThr";
		public static final String UP_CALM_TIME = "upCalmTime";
		public static final String BOT_CALM_TIME = "botCalmTime";
		public static final String UP_UP_CALM_TIME = "upUpCalmTime";
		public static final String BOT_BOT_CALM_TIME = "botBotCalmTime";
	}


}
